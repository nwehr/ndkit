//
//  main.cpp
//  NDProtocolParser
//
//  Created by Nathan Wehr on 4/15/13.
//  Copyright (c) 2013 Creltek. All rights reserved.
//

#include <iostream>
//#include <boost/asio.hpp>
//#include <boost/asio/ssl.hpp>
//#include <boost/bind.hpp>

#include <StringUtil/StdStringUtil.h>

namespace StringUtil = KHP; 
//
//#include "Parse/SMTP/SMTP.h"
//#include <b64/encode.h>
//
//#define DEFAULT_SMTP_BUFFER_SIZE 1024; 
//
//class SMTPClient {
//public:
//	SMTPClient( const std::string& i_Host, const std::string& i_Port, bool i_SSL = false )
//	: m_Host( i_Host )
//	, m_Port( i_Port )
//	, m_IOService()
//	, m_Resolver( m_IOService )
//	, m_Context( m_IOService, boost::asio::ssl::context::sslv23_client )
//	, m_Socket( m_IOService, m_Context )
//	, m_SSL( i_SSL )
//	, m_ReadBuffer( new char[1024] )
//	, m_WriteBuffer( new char[1024] )
//	, m_UsernameB64( "" )
//	, m_PasswordB64( "" )
//	, m_UsernameChallengeB64( "VXNlcm5hbWU6" )
//	, m_PasswordChallengeB64( "UGFzc3dvcmQ6" )
//	{}
//	
//	virtual ~SMTPClient(){
//		delete[] m_ReadBuffer;
//		delete[] m_WriteBuffer;
//		
//		// TODO: close socket if open.
//		
//	}
//	
//private:
//	void _HandleConnect( const boost::system::error_code& i_Error ){
//		if( !i_Error ){
//			if( m_SSL ){
//				m_Socket.async_handshake( boost::asio::ssl::stream_base::client
//										 , boost::bind( &SMTPClient::_HandleHandshake
//													   , this
//													   , boost::asio::placeholders::error ) );
//				
//				
//			} else _Send();
//			
//		} else {
//			std::cout << "Connect failed: " << i_Error.message() << "\n";
//			throw int( 5 );
//			
//		}
//				
//	}
//	
//	void _HandleHandshake( const boost::system::error_code& i_Error ){
//		if( !i_Error )
//			_Send();
//		else {
//			std::cout << "Handshake failed: " << i_Error.message() << std::endl;
//			throw int( 5 );
//			
//		}
//		
//	}
//	
//	void _HandleRead(){
//		
//	}
//	
//	void _HandleWrite(){
//		
//	}
//	
//	bool _VerifyCertificate( bool preverified, boost::asio::ssl::verify_context& ctx ){
//		char subject_name[256];
//		
//		X509* cert = X509_STORE_CTX_get_current_cert( ctx.native_handle() );
//		X509_NAME_oneline( X509_get_subject_name( cert ), subject_name, 256 );
//		
//		std::cout << "Verifying " << subject_name << std::endl;
//		
//	}
//	
//	void _Send(){
//		std::cout << ">> " << "EHLO nwehrmbp.wss.evrichart.com" << std::endl;
//		m_Socket.write_some( boost::asio::buffer( "EHLO nwehrmbp.wss.evrichart.com\n" ) );
//		
//		m_Socket.read_some( boost::asio::buffer( m_ReadBuffer, 1024 ) );
//		
//		std::cout << "<< " << m_ReadBuffer << std::endl;
//		
//		std::cout << ">> " << "EHLO nwehrmbp.wss.evrichart.com" << std::endl;
//		m_Socket.write_some( boost::asio::buffer( "EHLO nwehrmbp.wss.evrichart.com\n" ) );
//		
//		m_Socket.read_some( boost::asio::buffer( m_ReadBuffer, 1024 ) );
//		
//		std::cout << "<< " << m_ReadBuffer << std::endl;
//		
////		for( ;; ){
////			std::string Message;
////			
////			std::cout << "Message: ";
////			std::cin >> Message;
////			
////			m_Socket.write_some( boost::asio::buffer( Message.append( "\n" ) ) );
////			
////			char ReadBuffer[1024];
////			m_Socket.read_some( boost::asio::buffer( ReadBuffer, 1024 ) );
////			std::cout << "Response: " << ReadBuffer << std::endl;
////			
////		}
//		
//	}
//	
//public:
//	inline void SetMail( const std::string& i_Mail ){
//		m_Mail = i_Mail;
//	}
//	
//	inline void SetRcpt( const std::string& i_Rcpt ){
//		m_Rcpt = i_Rcpt;
//	}
//	
//	inline void SetData( const std::string& i_Data ){
//		m_Data = i_Data;
//	}
//	
//	void SetAuth( const std::string& i_Username, const std::string& i_Password ){
//		std::stringstream UsernameStream;
//		UsernameStream << i_Username;
//		
//		std::stringstream PasswordStream;
//		PasswordStream << i_Password;
//		
//		std::stringstream UsernameStreamB64;
//		std::stringstream PasswordStreamB64;
//		
//		base64::encoder Encoder;
//		
//		Encoder.encode( UsernameStream, UsernameStreamB64 );
//		Encoder.encode( PasswordStream, PasswordStreamB64 );
//		
//		m_UsernameB64 = UsernameStreamB64.str(); 
//		m_PasswordB64 = PasswordStreamB64.str();
//		
////		std::cout << i_Username << " >> " << m_UsernameB64;
////		std::cout << i_Password << " >> " << m_PasswordB64;
//		
//	}
//	
//	void Send(){
//		boost::asio::ip::tcp::resolver::query		Query( m_Host, m_Port );
//		boost::asio::ip::tcp::resolver::iterator	Iterator( m_Resolver.resolve( Query ) );
//		
//		if( m_SSL ){
//			m_Socket.set_verify_mode( boost::asio::ssl::verify_peer );
//			m_Socket.set_verify_callback( boost::bind( &SMTPClient::_VerifyCertificate, this, _1, _2 ) );
//			
//		}
//		
//		boost::asio::async_connect( m_Socket.lowest_layer()
//								   , Iterator
//								   , boost::bind( &SMTPClient::_HandleConnect
//												 , this
//												 , boost::asio::placeholders::error ) );
//		
//		m_IOService.run();
//		
//	}
//	
//private:
//	// Networking
//	std::string m_Host;
//	std::string m_Port;
//	
//	boost::asio::io_service			m_IOService;
//	boost::asio::ip::tcp::resolver	m_Resolver;
//	
//	boost::asio::ssl::context m_Context;
//	boost::asio::ssl::stream<boost::asio::ip::tcp::socket> m_Socket;
//	
//	bool m_SSL;
//	
//	char* m_ReadBuffer;
//	char* m_WriteBuffer; 
//	
//	// Email
//	std::string m_UsernameB64;
//	std::string m_PasswordB64;
//	
//	const std::string m_UsernameChallengeB64;
//	const std::string m_PasswordChallengeB64;
//	
//	std::string m_Mail;
//	std::string m_Rcpt;
//	std::string m_Data;
//	
//};

#include "Log/Logger.h"

#include <fstream>

int main( int argc, const char* argv[] ){
	// ehlo localhost
	// auth login
	// mail
	// rcpt
	// data
	// quit
	
//	SMTPClient MySMTPClient( "smtp.gmail.com", "465", true );
//	
//	MySMTPClient.SetAuth( "gtolemans", "hdrThun130" ); 
//	MySMTPClient.SetMail( "gtolemans@gmail.com" );
//	MySMTPClient.SetRcpt( "nathanw@evrichart.com" );
//	MySMTPClient.SetData( "Hello, World!" );
//	
//	MySMTPClient.Send();
	
	NDKit::Log::SeverityLogger MyLogger;
	
	MyLogger.SetSeverity( 2 );
	
	MyLogger.AddSink( new NDKit::Log::Sink::ConsoleSink() );
	MyLogger.AddSink( new NDKit::Log::Sink::FileSink( "/Users/nathanwehr/test.txt" ) );
	
	MyLogger.Log( 2, "Hello, World!" );
	
}
