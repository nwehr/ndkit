#ifndef _NDKit_Auth_LDAP_h
#define _NDKit_Auth_LDAP_h

//
//      Copyright 2013 Nathan Wehr. All Rights Reserved.
//
//      Redistribution and use in source and binary forms, with or without modification, are
//      permitted provided that the following conditions are met:
//
//              1. Redistributions of source code must retain the above copyright notice, this list of
//              conditions and the following disclaimer.
//
//              2. Redistributions in binary form must reproduce the above copyright notice, this list
//              of conditions and the following disclaimer in the documentation and/or other materials
//              provided with the distribution.
//
//      THIS SOFTWARE IS PROVIDED BY NATHAN WEHR ''AS IS'' AND ANY EXPRESS OR IMPLIED
//      WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
//      FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL NATHAN WEHR OR
//      CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
//      CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
//      SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
//      ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//      NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
//      ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//      The views and conclusions contained in the software and documentation are those of the
//      authors and should not be interpreted as representing official policies, either expressed
//      or implied, of Nathan Wehr.
//

// C
#include <ldap.h>

// C++
#include <vector>
#include <sstream>

// Internal
#include <NDkit/Auth/Auth.h>

namespace NDkit {
	namespace Auth {
		class LDAPAuthenticator : Authenticator {
			LDAPAuthenticator();
			
		public:
			explicit LDAPAuthenticator( const std::string& i_Host
							  , unsigned int i_Port
							  , const std::string& i_UserDN
							  , const std::string& i_GroupDN
							  , const std::vector<std::string>& i_UserFilter
							  , const std::vector<std::string>& i_GroupFilter )
			: m_Host( i_Host )
			, m_Port( i_Port )
			, m_UserDN( i_UserDN )
			, m_GroupDN( i_GroupDN )
			, m_UserFilter( i_UserFilter )
			, m_GroupFilter( i_GroupFilter )
			{
				std::stringstream URLStream;
				URLStream << "ldap://" << m_Host << ":" << m_Port;
				
				if( ldap_initialize( &m_Handle, URLStream.str().c_str() ) ){
					
				}
				
			}
			
			virtual ~LDAPAuthenticator(){}
				   
			virtual Authenticate( const std::string& i_User, const std::string& i_Password ) {
				
			}
			
		private:
			std::string m_Host;
			unsigned in m_Port;
			
			std::string m_UserDN;
			std::string m_GroupDN;
			
			std::vector<std::string> m_UserFilter;
			std::vector<std::string> m_GroupFilter;
			
			LDAP* m_Handle; 
			
		};
		
	} // namespace Auth
	
} // namespace NDKit

#endif // _NDKit_Auth_LDAP_h
