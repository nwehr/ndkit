#ifndef _NDProtocolParser_WebSocket_Frame_h
#define _NDProtocolParser_WebSocket_Frame_h

//
//      Copyright 2013 Nathan Wehr. All Rights Reserved.
//      Copyright 2013 EvriChart. All Rights Reserved.
//
//      Redistribution and use in source and binary forms, with or without modification, are
//      permitted provided that the following conditions are met:
//
//              1. Redistributions of source code must retain the above copyright notice, this list of
//              conditions and the following disclaimer.
//
//              2. Redistributions in binary form must reproduce the above copyright notice, this list
//              of conditions and the following disclaimer in the documentation and/or other materials
//              provided with the distribution.
//
//      THIS SOFTWARE IS PROVIDED BY NATHAN WEHR ''AS IS'' AND ANY EXPRESS OR IMPLIED
//      WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
//      FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL NATHAN WEHR OR
//      CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
//      CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
//      SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
//      ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//      NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
//      ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//      The views and conclusions contained in the software and documentation are those of the
//      authors and should not be interpreted as representing official policies, either expressed
//      or implied, of Nathan Wehr.
//

#include <cstdint>
#include <string>

namespace NDKit {
	namespace Parse {
		namespace WebSocket {
			///////////////////////////////////////////////////////////////////////////////
			// Frame
			///////////////////////////////////////////////////////////////////////////////
			class Frame {
			public:
				Frame()
				: m_FIN( 0 )
				, m_RSV1( 0 )
				, m_RSV2( 0 )
				, m_RSV3( 0 )
				, m_Opcode( 0 )
				, m_Mask( 0 )
				, m_MaskKey( new uint8_t[4] )
				, m_PLength( 0 )
				, m_Payload( std::string() )
				, m_Buffer( 0 )
				{
					
				}
				
				Frame( const char* i_Buffer )
				: m_FIN( 0 )
				, m_RSV1( 0 )
				, m_RSV2( 0 )
				, m_RSV3( 0 )
				, m_Opcode( 0 )
				, m_Mask( 0 )
				, m_MaskKey( new uint8_t[4] )
				, m_PLength( 0 )
				, m_Payload( std::string() )
				, m_Buffer( 0 )
				{
					m_FIN 		= +i_Buffer[0] & 128;
					m_RSV1 		= +i_Buffer[0] & 64;
					m_RSV2 		= +i_Buffer[0] & 32;
					m_RSV3 		= +i_Buffer[0] & 16;
					m_Opcode 	= +i_Buffer[0] & 15;
					
					if( i_Buffer[1] ){
						m_Mask 		= +i_Buffer[1] & 128;
						m_PLength 	= +i_Buffer[1] & 127;
						
						uint32_t MaskKeyIndex( 2 );
						uint32_t PayloadIndex( 2 + (m_Mask ? 4 : 0) );
						
						if( m_PLength > 125 ){
							m_PLength = 0;
							
							switch( m_PLength ){
								case 126: // Next two bytes represent the payload length
									MaskKeyIndex += 2;
									PayloadIndex += 2;
									
									m_PLength = m_PLength | static_cast<uint64_t>( i_Buffer[2] << 8 );
									m_PLength = m_PLength | static_cast<uint64_t>( i_Buffer[3] );
									
									break;
								case 127: // Next eight bytes represent the payload length
									MaskKeyIndex += 8;
									PayloadIndex += 8;
									
									uint32_t BitLength( 64 );
									
									for( uint32_t i = 0; i < 8; ++i )
										m_PLength = m_PLength | static_cast<uint64_t>( i_Buffer[i + 2] ) << (BitLength -= 8);
									
									break;
									
							}
							
						}
						
						if( m_Mask ){
							for( uint32_t i = 0; i < 4; ++i )
								m_MaskKey[i] = +i_Buffer[MaskKeyIndex + i];
							
							for( uint32_t i = 0; i < m_PLength; ++i )
								m_Payload += i_Buffer[PayloadIndex + i] ^ +m_MaskKey[i % 4];
							
						} else {
							for( uint32_t i = 0; i < m_PLength; ++i )
								m_Payload += i_Buffer[PayloadIndex + i];
							
						}
						
					}
					
				}
				
				virtual ~Frame(){
					if( m_Buffer )
						delete[] m_Buffer;
					
					delete[] m_MaskKey;
					
				}
				
				const uint32_t GetBufferSize() const {
					uint32_t Size( 1 );
					
					if( m_Mask || m_PLength ){
						Size += 1;
						
						if( m_PLength ){
							if( m_PLength > 125 ){
								if( m_PLength > 65535 ) Size += 8;
								else Size += 2;
								
							}
							
							Size += m_Payload.size();
							
						}
						
						if( m_Mask )
							Size += 4;
						
						
					}
					
					return Size;
					
				}
				
				void SetBuffer( char* io_Buffer ){
					for( uint32_t i = 0; i < GetBufferSize(); ++i )
						io_Buffer[i] = 0;
					
					io_Buffer[0] = io_Buffer[0] | static_cast<char>( m_FIN ) << 7;
					io_Buffer[0] = io_Buffer[0] | static_cast<char>( m_RSV1 ) << 6;
					io_Buffer[0] = io_Buffer[0] | static_cast<char>( m_RSV2 ) << 5;
					io_Buffer[0] = io_Buffer[0] | static_cast<char>( m_RSV3 ) << 4;
					io_Buffer[0] = io_Buffer[0] | m_Opcode;
					
					if( GetBufferSize() > 1 ){
						io_Buffer[1] = io_Buffer[1] | static_cast<char>( m_Mask ) << 7;
						
						uint32_t MaskKeyIndex( 2 );
						uint32_t PayloadIndex( 2 + (m_Mask ? 4 : 0) );
						
						if( m_PLength > 125 ){
							if( m_PLength > 65535 ){
								io_Buffer[1] = io_Buffer[1] | 127;
								
								MaskKeyIndex += 8;
								PayloadIndex += 8;
								
								for( uint32_t i = 0; i < 8; ++i )
									io_Buffer[i + 2] = reinterpret_cast<char*>( &m_PLength )[i];
								
							} else {
								io_Buffer[1] = io_Buffer[1] | 126;
								
								MaskKeyIndex += 2;
								PayloadIndex += 2;
								
								io_Buffer[2] = reinterpret_cast<char*>( &m_PLength )[6];
								io_Buffer[3] = reinterpret_cast<char*>( &m_PLength )[7];
								
							}
							
						} else io_Buffer[1] = io_Buffer[1] | m_PLength;
						
						if( m_Mask ){
							for( uint32_t i = 0; i < 4; ++i )
								io_Buffer[MaskKeyIndex + i] = reinterpret_cast<char*>( m_MaskKey )[i];
							
							for( uint32_t i = 0; i < m_PLength; ++i )
								io_Buffer[PayloadIndex + i] = m_Payload[i] ^ +m_MaskKey[i % 4];
							
						} else {
							for( uint32_t i = 0; i < m_PLength; ++i )
								io_Buffer[PayloadIndex + i] = m_Payload[i];
							
						}
						
					}
					
				}
				
				const char* GetBuffer(){
					if( m_Buffer )
						delete[] m_Buffer;
					
					m_Buffer = new char[GetBufferSize()];
					
					SetBuffer( m_Buffer );
					
					return m_Buffer;
					
				}
				
				inline void SetFIN( bool i_FIN ){ m_FIN = i_FIN; }
				
				inline void SetRSV1( bool i_RSV1 ){ m_RSV1 = i_RSV1; }
				inline void SetRSV2( bool i_RSV2 ){ m_RSV2 = i_RSV2; }
				inline void SetRSV3( bool i_RSV3 ){ m_RSV3 = i_RSV3; }
				
				inline void SetMask( bool i_Mask ){ m_Mask = i_Mask; }
				
				inline void SetOpcode( uint8_t i_Opcode ){ m_Opcode = i_Opcode; }
				
				inline void SetPayload( const std::string& i_Payload ){
					m_Payload = i_Payload;
					m_PLength = m_Payload.size();
					
				}
				
				inline void SetMaskKey( uint32_t i_MaskKey ){
					for( uint32_t i = 0; i < 4; ++i )
						m_MaskKey[i] = reinterpret_cast<uint8_t*>( &i_MaskKey )[i];
					
				}
				
				inline bool FIN(){ return m_FIN; }
				
				inline bool RSV1(){ return m_RSV1; }
				inline bool RSV2(){ return m_RSV2; }
				inline bool RSV3(){ return m_RSV3; }
				
				inline bool Mask(){ return m_Mask; }
				
				inline uint8_t* MaskKey(){ return m_MaskKey; }
				
				inline uint8_t Opcode(){ return m_Opcode; }
				
				inline uint64_t PLength(){ return m_PLength; }
				
				inline std::string Payload(){ return m_Payload; }
				
			private:
				bool m_FIN;
				
				bool m_RSV1;
				bool m_RSV2;
				bool m_RSV3;
				
				uint8_t m_Opcode;
				
				bool m_Mask;
				
				uint8_t* m_MaskKey;
				uint64_t m_PLength;
				
				std::string m_Payload;
				
				char* m_Buffer;
				
			};
			
		} // namespace WebSocket
		
	} // namespace Parse
	
} // namespace NDKit

#endif // _NDProtocolParser_WebSocket_Frame_h
