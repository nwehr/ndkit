#ifndef _NDKit_Parse_REST_REST_h
#define _NDKit_Parse_REST_REST_h

//
//      Copyright 2012-2013 Nathan Wehr. All Rights Reserved.
//      Copyright 2013 EvriChart. All Rights Reserved.
//
//      Redistribution and use in source and binary forms, with or without modification, are
//      permitted provided that the following conditions are met:
//
//              1. Redistributions of source code must retain the above copyright notice, this list of
//              conditions and the following disclaimer.
//
//              2. Redistributions in binary form must reproduce the above copyright notice, this list
//              of conditions and the following disclaimer in the documentation and/or other materials
//              provided with the distribution.
//
//      THIS SOFTWARE IS PROVIDED BY NATHAN WEHR ''AS IS'' AND ANY EXPRESS OR IMPLIED
//      WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
//      FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL NATHAN WEHR OR
//      CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
//      CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
//      SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
//      ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//      NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
//      ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//      The views and conclusions contained in the software and documentation are those of the
//      authors and should not be interpreted as representing official policies, either expressed
//      or implied, of Nathan Wehr.
//

// C++
#include <string>
#include <sstream>
#include <vector>
#include <exception>

#include <iostream>

// External
#include "StringUtil/StdStringUtil.h"

namespace StringUtil = KHP;

namespace NDKit {
	namespace Parse {
		namespace REST {
			///////////////////////////////////////////////////////////////////////////////
			// Header
			///////////////////////////////////////////////////////////////////////////////
			class Header {
			public:
				Header()
				: m_Key( std::string() )
				, m_Val( std::string() )
				{}
				
				Header( const std::string& i_Key, const std::string& i_Val )
				: m_Key( i_Key )
				, m_Val( i_Val )
				{}
				
				Header( const NDKit::Parse::REST::Header& i_Header )
				: m_Key( i_Header.Key() )
				, m_Val( i_Header.Val() )
				{}
				
				virtual ~Header(){}
				
				inline std::string& Key(){
					return m_Key;
				}
				
				inline const std::string& Key() const {
					return m_Key;
				}
				
				inline std::string& Val(){
					return m_Val;
				}
				
				inline const std::string& Val() const {
					return m_Val;
				}
				
			protected:
				std::string m_Key;
				std::string m_Val;
				
			};
			
			///////////////////////////////////////////////////////////////////////////////
			// Transport
			///////////////////////////////////////////////////////////////////////////////
			class Transport {
			public:
				Transport()
				: m_Headers( std::vector<NDKit::Parse::REST::Header>() )
				, m_Body( std::string() )
				{}
				
				Transport( const std::vector<NDKit::Parse::REST::Header>& i_Headers, const std::string& i_Body )
				: m_Headers( i_Headers )
				, m_Body( i_Body )
				{}
				
				Transport( const NDKit::Parse::REST::Transport& i_Transport )
				: m_Headers( i_Transport.Headers() )
				, m_Body( i_Transport.Body() )
				{}
				
				virtual ~Transport(){}
				
				std::vector<NDKit::Parse::REST::Header>& Headers(){
					return m_Headers;
				}
				
				inline const std::vector<NDKit::Parse::REST::Header>& Headers() const {
					return m_Headers;
				}
				
				NDKit::Parse::REST::Header& Header( const std::string& i_Key ){
					for( unsigned int i = 0; i < m_Headers.size(); ++i ){
						if( i_Key == m_Headers[i].Key() )
							return m_Headers[i];
						
					}
					
					// Throw something more reasonable...?
					throw std::string( "header not found" );
					
				}
				
				const NDKit::Parse::REST::Header& Header( const std::string& i_Key ) const {
					for( unsigned int i = 0; i < m_Headers.size(); ++i ){
						if( i_Key == m_Headers[i].Key() )
							return m_Headers[i];
						
					}
					
					// Throw something more reasonable...?
					throw std::string( "header not found" );
					
				}
				
				inline std::string& Body(){
					return m_Body;
				}
				
				inline const std::string& Body() const {
					return m_Body;
				}
				
				virtual const std::string Buffer() const = 0;
				
			protected:
				std::vector<NDKit::Parse::REST::Header> m_Headers;
				std::string m_Body;
				
			};
			
			///////////////////////////////////////////////////////////////////////////////
			// Request
			///////////////////////////////////////////////////////////////////////////////
			class Request : public Transport {
			public:
				Request()
				: Transport()
				, m_Verb( std::string( "GET" ) )
				, m_URI( std::string( "/" ) )
				, m_Version( std::string( "HTTP/1.1" ) )
				{}
				
				Request( const std::string& i_Verb
						, const std::string& i_URI
						, const std::string& i_Version
						, const std::vector<NDKit::Parse::REST::Header>& i_Headers
						, const std::string& i_Body )
				: Transport( i_Headers, i_Body )
				, m_Verb( i_Verb )
				, m_URI( i_URI )
				, m_Version( i_Version )
				{}
				
				Request( const NDKit::Parse::REST::Request& i_Request )
				: Transport( i_Request.Headers(), i_Request.Body() )
				, m_Verb( i_Request.Verb() )
				, m_URI( i_Request.URI() )
				, m_Version( i_Request.Version() )
				{}
				
				Request( const std::string& i_Buffer )
				: Transport()
				, m_Verb( std::string() )
				, m_URI( std::string() )
				, m_Version( std::string() )
				{
					std::vector<std::string> LineTokens;
					
					StringUtil::explode( i_Buffer, LineTokens, std::string( "\r\n" ) );
					
					bool BlankLine = false;
					
					if( LineTokens.size() ){
						for( unsigned int i = 0; i < LineTokens.size(); ++i ){
							if( i == 0 ){
								std::vector<std::string> MetaTokens;
								
								StringUtil::explode( LineTokens[i], MetaTokens, std::string( " " ) );
								
								if( MetaTokens.size() == 3 ){
									m_Verb = MetaTokens[0];
									m_URI = MetaTokens[1];
									m_Version = MetaTokens[2];
									
								} else throw std::string( "Not a REST Protocol: invalid type" );
								
							} else {
								if( !BlankLine ){
									if( LineTokens[i] != "" ){
										std::vector<std::string> HeaderTokens;
										
										StringUtil::explode( LineTokens[i], HeaderTokens, std::string( ":" ) );
										
										if( HeaderTokens.size() )
											m_Headers.push_back( NDKit::Parse::REST::Header( StringUtil::trim( HeaderTokens[0] ), StringUtil::trim( HeaderTokens[1] ) ) );
										else
											throw std::string( "Not a REST Protocol: invalid headers" );
										
									} else BlankLine = true;
									
								} else {
									m_Body = LineTokens[i];
									
									break;
									
								}
								
							}
							
						}
						
					} else {
						throw std::string( "Empty Buffer" );
						
					}
					
				}
				
				Request& operator=( const Request& i_Request ){
					if( *this != i_Request ){
						m_Headers	= i_Request.Headers();
						m_Body		= i_Request.Body();
						m_Verb		= i_Request.Verb();
						m_URI		= i_Request.URI();
						m_Version	= i_Request.Version();
						
					}
					
					return *this;
					
				}
				
				bool operator==( const Request& i_Request ){
					return ( m_Body			== i_Request.Body()
							&& m_Verb		== i_Request.Verb()
							&& m_URI		== i_Request.URI()
							&& m_Version	== i_Request.Version() );
					
				}
				
				bool operator!=( const Request& i_Request ){
					return !operator==( i_Request ); 
				}
				
				virtual ~Request(){}
				
				inline std::string& Verb(){
					return m_Verb;
				}
				
				inline const std::string& Verb() const {
					return m_Verb;
				}
				
				inline std::string& URI(){
					return m_URI;
				}
				
				inline const std::string& URI() const {
					return m_URI;
				}
				
				inline std::string& Version(){
					return m_Version;
				}
				
				inline const std::string& Version() const {
					return m_Version;
				}
				
				virtual const std::string Buffer() const {
					std::stringstream BufferStream;
					
					BufferStream << m_Verb << " " << m_URI << " " << m_Version << "\r\n";
					
					for( unsigned int i = 0; i < m_Headers.size(); ++i )
						BufferStream << m_Headers[i].Key() << ": " << m_Headers[i].Val() << "\r\n";
					
					BufferStream << "\r\n";
					
					if( m_Body.size() )
						BufferStream << m_Body << "\r\n";
					
					return BufferStream.str();
					
				}
				
			protected:
				std::string m_Verb;
				std::string m_URI;
				std::string m_Version;
				
			};
			
			///////////////////////////////////////////////////////////////////////////////
			// Response
			///////////////////////////////////////////////////////////////////////////////
			class Response : public Transport {
			public:
				Response()
				: Transport()
				, m_Version( std::string() )
				, m_Code( std::string() )
				, m_Title( std::string() )
				{}
				
				Response( const std::string& i_Version
						 , const std::string& i_Code
						 , const std::string& i_Title )
				: Transport()
				, m_Version( i_Version )
				, m_Code( i_Code )
				, m_Title( i_Title )
				{}
				
				Response( const NDKit::Parse::REST::Response& i_Response )
				: Transport()
				, m_Version( i_Response.Version() )
				, m_Code( i_Response.Code() )
				, m_Title( i_Response.Title() )
				{}
				
				Response( const std::string& i_Buffer )
				:Transport()
				, m_Version( std::string() )
				, m_Code( std::string() )
				, m_Title( std::string() )
				{
					std::vector<std::string> LineTokens;
					
					StringUtil::explode( i_Buffer, LineTokens, std::string( "\r\n" ) );
					
					bool BlankLine = false;
					
					if( LineTokens.size() ){
						for( unsigned int i = 0; i < LineTokens.size(); ++i ){
							if( i == 0 ){
								std::vector<std::string> MetaTokens;
								
								StringUtil::explode( LineTokens[i], MetaTokens, std::string( " " ) );
								
								if( MetaTokens.size() == 3 ){
									m_Version	= MetaTokens[0];
									m_Code		= MetaTokens[1];
									m_Title		= MetaTokens[2];
									
								} else throw std::string( "Not a REST Protocol: invalid type" );
								
							} else {
								if( !BlankLine ){
									if( LineTokens[i] != "" ){
										std::vector<std::string> HeaderTokens;
										
										StringUtil::explode( LineTokens[i], HeaderTokens, std::string( ":" ) );
										
										if( HeaderTokens.size() )
											m_Headers.push_back( NDKit::Parse::REST::Header( StringUtil::trim( HeaderTokens[0] ), StringUtil::trim( HeaderTokens[1] ) ) );
										else
											throw std::string( "Not a REST Protocol: invalid headers" );
										
									} else BlankLine = true;
									
								} else {
									while( i < LineTokens.size() ){
										m_Body.append( LineTokens[i] ).append( "\n" );
										++i; 
										
									}
									
									break;
									
								}
								
							}
							
						}
						
					} else {
						throw std::string( "Empty Buffer" );
						
					}
					
				}
				
				Response& operator=( const Response& i_Response ){
					if( *this != i_Response ){
						m_Headers	= i_Response.Headers();
						m_Body		= i_Response.Body();
						m_Version	= i_Response.Version();
						m_Code		= i_Response.Code();
						m_Title		= i_Response.Title(); 
						
					}
					
					return *this;
					
				}
				
				bool operator==( const Response& i_Response ){
					return ( m_Body			== i_Response.Body()
							&& m_Version	== i_Response.Version()
							&& m_Code		== i_Response.Code()
							&& m_Title		== i_Response.Title() );
					
				}
				
				bool operator!=( const Response& i_Response ){
					return !operator==( i_Response );
				}
				
				virtual ~Response(){}
				
				inline std::string& Version(){
					return m_Version;
				}
				
				inline const std::string& Version() const {
					return m_Version;
				}
				
				inline std::string& Code(){
					return m_Code;
				}
				
				inline const std::string& Code() const {
					return m_Code;
				}
				
				inline std::string& Title(){
					return m_Title;
				}
				
				inline const std::string& Title() const {
					return m_Title;
				}
				
				virtual const std::string Buffer() const {
					std::stringstream BufferStream;
					
					BufferStream << m_Version << " " << m_Code << " " << m_Title << "\r\n";
					
					for( unsigned int i = 0; i < m_Headers.size(); ++i )
						BufferStream << m_Headers[i].Key() << ": " << m_Headers[i].Val() << "\r\n";
					
					BufferStream << "\r\n";
					
					if( m_Body.size() )
						BufferStream << m_Body << "\r\n";
					
					return BufferStream.str();
					
				}
				
			protected:
				std::string m_Version;
				std::string m_Code;
				std::string m_Title;
				
			};
			
		} // namespace REST
		
	} // namespace Parse
	
} // namespace NDKit

#endif // define _NDKit_Parse_REST_REST_h

