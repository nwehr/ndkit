#ifndef _NDKit_Parse_SMTP_SMTP_h
#define _NDKit_Parse_SMTP_SMTP_h

//
//      Copyright 2013 Nathan Wehr. All Rights Reserved.
//
//      Redistribution and use in source and binary forms, with or without modification, are
//      permitted provided that the following conditions are met:
//
//              1. Redistributions of source code must retain the above copyright notice, this list of
//              conditions and the following disclaimer.
//
//              2. Redistributions in binary form must reproduce the above copyright notice, this list
//              of conditions and the following disclaimer in the documentation and/or other materials
//              provided with the distribution.
//
//      THIS SOFTWARE IS PROVIDED BY NATHAN WEHR ''AS IS'' AND ANY EXPRESS OR IMPLIED
//      WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
//      FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL NATHAN WEHR OR
//      CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
//      CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
//      SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
//      ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//      NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
//      ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//      The views and conclusions contained in the software and documentation are those of the
//      authors and should not be interpreted as representing official policies, either expressed
//      or implied, of Nathan Wehr.
//

// C++
#include <string>
#include <sstream>

namespace NDKit {
	namespace Parse {
		namespace SMTP {
			namespace Command {
				///////////////////////////////////////////////////////////////////////////////
				// CMMD
				///////////////////////////////////////////////////////////////////////////////
				class CMMD {
				public:
					CMMD()
					: m_Command( "" )
					, m_Argument( "" )
					{}
					
					CMMD( const std::string& i_Command, const std::string& i_Argument )
					: m_Command( i_Command )
					, m_Argument( i_Argument )
					{}
					
					CMMD( const std::string& i_Buffer )
					: m_Command( "" )
					, m_Argument( "" )
					{
						// Read characters until I get to a space or \n
						for( size_t i = 0; i < i_Buffer.size(); ++i ){
							if( i_Buffer[i] == ' ' || i_Buffer[i] == '\n' ){
								m_Command = i_Buffer.substr( 0, i );
								m_Argument = i_Buffer.substr( i + 1, i_Buffer.size() );
								
								break;
								
							}
							
						}
						
					}
					
					virtual ~CMMD(){}
					
					std::string& Command(){
						return m_Command;
					}
					
					const std::string& Command() const {
						return m_Command;
					}
					
					std::string& Argument(){
						return m_Argument;
					}
					
					const std::string& Argument() const {
						return m_Argument;
					}
					
					virtual std::string Buffer(){
						std::stringstream BufferStream;
						
						BufferStream << m_Command << (m_Argument.length() ? std::string( " " ).append( m_Argument ) : "") << "\n";
						
						return BufferStream.str();
						
					}
					
					bool IsHELO(){
						return (m_Command == "HELO");
					}
					
					bool IsEHLO(){
						return (m_Command == "EHLO");
					}
					
					bool IsMAIL(){
						return (m_Command == "MAIL FROM:");
					}
					
					bool IsRCPT(){
						return (m_Command == "RCPT TO:");
					}
					
					bool IsDATA(){
						return (m_Command == "DATA");
					}
					
					bool IsQUIT(){
						return (m_Command == "QUIT");
					}
					
					bool IsSTARTTLS(){
						return (m_Command == "STARTTLS");
					}
					
				private:
					std::string m_Command;
					std::string m_Argument;
					
				};
				
				///////////////////////////////////////////////////////////////////////////////
				// HELO
				///////////////////////////////////////////////////////////////////////////////
				class HELO : public CMMD {
				public:
					HELO()
					: CMMD( "HELO", "localhost" )
					{}
					
					HELO( const std::string& i_Argument )
					: CMMD( "HELO", i_Argument )
					{}
					
					HELO( const CMMD& Cmd )
					: CMMD( Cmd.Command(), Cmd.Argument() )
					{}
					
					virtual ~HELO(){}
										
				};
				
				///////////////////////////////////////////////////////////////////////////////
				// EHLO
				///////////////////////////////////////////////////////////////////////////////
				class EHLO : public CMMD {
				public:
					EHLO()
					: CMMD( "EHLO", "localhost" )
					{}
					
					EHLO( const std::string& i_Argument )
					: CMMD( "EHLO", i_Argument )
					{}
					
					EHLO( const CMMD& Cmd )
					: CMMD( Cmd.Command(), Cmd.Argument() )
					{}
					
					virtual ~EHLO(){}
					
				};
				
				///////////////////////////////////////////////////////////////////////////////
				// MAIL
				///////////////////////////////////////////////////////////////////////////////
				class MAIL : public CMMD {
				public:
					MAIL()
					: CMMD( "MAIL FROM:", "<noreply@localhost>" )
					{}
					
					MAIL( const std::string& i_Argument )
					: CMMD( "MAIL FROM:", i_Argument )
					{}
					
					MAIL( const CMMD& Cmd )
					: CMMD( Cmd.Command(), Cmd.Argument() )
					{}
					
					virtual ~MAIL(){}
					
				};
				
				///////////////////////////////////////////////////////////////////////////////
				// RCPT
				///////////////////////////////////////////////////////////////////////////////
				class RCPT : public CMMD {
				public:
					RCPT()
					: CMMD( "RCPT TO:", "<someone@localhost>" )
					{}
					
					RCPT( const std::string& i_Argument )
					: CMMD( "RCPT TO:", i_Argument )
					{}
					
					RCPT( const CMMD& Cmd )
					: CMMD( Cmd.Command(), Cmd.Argument() )
					{}
					
					virtual ~RCPT(){}
					
				};
				
				///////////////////////////////////////////////////////////////////////////////
				// DATA
				///////////////////////////////////////////////////////////////////////////////
				class DATA : public CMMD {
				public:
					DATA()
					: CMMD( "DATA", "" )
					, m_Subject( "" )
					, m_Message( "" )
					{}
					
					DATA( const std::string& i_Subject, const std::string& i_Message )
					: CMMD( "DATA", "" )
					, m_Subject( i_Subject )
					, m_Message( i_Message )
					{}
					
					DATA( const CMMD& Cmd )
					: CMMD( Cmd.Command(), "" )
					, m_Subject( "" )
					, m_Message( "" )
					{
						// TODO: parse argument
						
						
					}
					
					virtual ~DATA(){}
					
					virtual std::string Buffer(){
						std::stringstream BufferStream;
						
						BufferStream
						<< "DATA"
						<< "\n"
						<< "Subject: "
						<< m_Subject
						<< "\n\n"
						<< m_Message
						<< "\n.";
						
						return BufferStream.str();
						
					}
					
				private:
					std::string m_Subject;
					std::string m_Message;
					
				};
				
				///////////////////////////////////////////////////////////////////////////////
				// QUIT
				///////////////////////////////////////////////////////////////////////////////
				class QUIT : public CMMD {
				public:
					QUIT()
					: CMMD( "QUIT", "" )
					{}
					
					QUIT( const CMMD& Cmd )
					: CMMD( Cmd.Command(), "" )
					{}
					
					virtual ~QUIT(){}
					
				};
				
				///////////////////////////////////////////////////////////////////////////////
				// STARTTLS
				///////////////////////////////////////////////////////////////////////////////
				class STARTTLS : public CMMD {
				public:
					STARTTLS()
					: CMMD( "STARTTLS", "" )
					{}
					
					STARTTLS( const CMMD& Cmd )
					: CMMD( Cmd.Command(), "" )
					{}
					
					virtual ~STARTTLS(){}
					
				};
				
			} // namespace Command
			
			namespace Response {
				///////////////////////////////////////////////////////////////////////////////
				// RESP
				///////////////////////////////////////////////////////////////////////////////
				class RESP {
				public:
					RESP()
					: m_Code( 0 )
					, m_Message( "" )
					{}
					
					RESP( int i_Code, const std::string& i_Message )
					: m_Code( i_Code )
					, m_Message( i_Message )
					{}
					
					RESP( const std::string& i_Buffer )
					: m_Code( 0 )
					, m_Message( "" )
					{
						m_Code		= atoi( i_Buffer.substr( 0, 3 ).c_str() );
						m_Message	= i_Buffer.substr( 4, i_Buffer.length() );
						
					}
					
					virtual ~RESP(){}
					
					int Code(){
						return m_Code;
					}
					
					const int Code() const {
						return m_Code;
					}
					
					std::string& Message(){
						return m_Message;
					}
					
					const std::string& Message() const {
						return m_Message;
					}
					
					virtual std::string Buffer( bool i_WithHyphen = false ){
						std::stringstream BufferStream;
						
						BufferStream << m_Code << (i_WithHyphen ? "-" : " ") << m_Message << "\n";
						
						return BufferStream.str();
						
					}
					
				private:
					int			m_Code;
					std::string m_Message; 
					
				};
				
			} // namespace Response
			
		} // namespace SMTP
		
	} // namespace Parse
	
} // namespace NDKit

#endif // _NDKit_Parse_SMTP_SMTP_h
