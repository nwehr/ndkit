#ifndef _NDKit_Log_Sink_h
#define _NDKit_Log_Sink_h

//
//      Copyright 2013 Nathan Wehr. All Rights Reserved.
//
//      Redistribution and use in source and binary forms, with or without modification, are
//      permitted provided that the following conditions are met:
//
//              1. Redistributions of source code must retain the above copyright notice, this list of
//              conditions and the following disclaimer.
//
//              2. Redistributions in binary form must reproduce the above copyright notice, this list
//              of conditions and the following disclaimer in the documentation and/or other materials
//              provided with the distribution.
//
//      THIS SOFTWARE IS PROVIDED BY NATHAN WEHR ''AS IS'' AND ANY EXPRESS OR IMPLIED
//      WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
//      FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL NATHAN WEHR OR
//      CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
//      CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
//      SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
//      ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//      NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
//      ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//      The views and conclusions contained in the software and documentation are those of the
//      authors and should not be interpreted as representing official policies, either expressed
//      or implied, of Nathan Wehr.
//

// C++
#include <iostream>
#include <fstream>

#include <string>

namespace NDKit {
	namespace Log {
		namespace Sink {
			///////////////////////////////////////////////////////////////////////////////
			// BaseSink
			///////////////////////////////////////////////////////////////////////////////
			class BaseSink {
			public:
				virtual ~BaseSink() {}
				virtual void Write( const std::string& ) = 0;
				
			}; 
			
			///////////////////////////////////////////////////////////////////////////////
			// ConsoleSink
			///////////////////////////////////////////////////////////////////////////////
			class ConsoleSink : public BaseSink {
			public:
				virtual ~ConsoleSink() {}
				
				virtual void Write( const std::string& i_Message ) {
					std::cout << i_Message << std::endl;
				}
				
			}; 
			
			///////////////////////////////////////////////////////////////////////////////
			// FileSink
			///////////////////////////////////////////////////////////////////////////////
			class FileSink : public BaseSink {
			public:
				FileSink( const std::string& i_FilePath )
				: m_FilePath( i_FilePath )
				, m_File()
				{}
				
				virtual ~FileSink() {}
				
				virtual void Write( const std::string& i_Message ) {
					m_File.open( m_FilePath.c_str(), std::ofstream::out | std::ofstream::app );
					
					m_File << i_Message << std::endl;
					
					m_File.close();
					
				}
				
			private:
				std::string		m_FilePath;
				std::ofstream	m_File;
				
			};
			
		} // namespace Sink
		
	} // namespace Log
	
} // namespace NDKit

#endif // _NDKit_Log_Sink_h
