#ifndef _NDKit_Log_Logger_h
#define _NDKit_Log_Logger_h

//
//      Copyright 2013 Nathan Wehr. All Rights Reserved.
//
//      Redistribution and use in source and binary forms, with or without modification, are
//      permitted provided that the following conditions are met:
//
//              1. Redistributions of source code must retain the above copyright notice, this list of
//              conditions and the following disclaimer.
//
//              2. Redistributions in binary form must reproduce the above copyright notice, this list
//              of conditions and the following disclaimer in the documentation and/or other materials
//              provided with the distribution.
//
//      THIS SOFTWARE IS PROVIDED BY NATHAN WEHR ''AS IS'' AND ANY EXPRESS OR IMPLIED
//      WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
//      FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL NATHAN WEHR OR
//      CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
//      CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
//      SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
//      ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//      NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
//      ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//      The views and conclusions contained in the software and documentation are those of the
//      authors and should not be interpreted as representing official policies, either expressed
//      or implied, of Nathan Wehr.
//

// C++
#include <string>
#include <vector>

#include <NDKit/Log/Sink.h>

namespace NDKit {
	namespace Log {
		///////////////////////////////////////////////////////////////////////////////
		// BaseLogger
		///////////////////////////////////////////////////////////////////////////////
		class BaseLogger {
		public:
			virtual ~BaseLogger() {
				for( size_t i = 0; i < m_Sinks.size(); ++i )
					delete m_Sinks[i];
				
			}
			
			virtual void Log( const std::string& ) = 0;
			
			void AddSink( Sink::BaseSink* i_Sink ) {
				m_Sinks.push_back( i_Sink ); 
			}
			
		protected:
			std::vector<Sink::BaseSink*> m_Sinks;
			
		};
		
		
		///////////////////////////////////////////////////////////////////////////////
		// BasicLogger
		///////////////////////////////////////////////////////////////////////////////
		class BasicLogger : public BaseLogger {
		public:
			virtual ~BasicLogger() {}
			
			virtual void Log( const std::string& i_Message ) {
				for( std::vector<Sink::BaseSink*>::iterator it = m_Sinks.begin(); it != m_Sinks.end(); ++it ){
					(*it)->Write( i_Message );
				}
				
			}
			
		};
		
		///////////////////////////////////////////////////////////////////////////////
		// SeverityLogger
		///////////////////////////////////////////////////////////////////////////////
		class SeverityLogger : public BaseLogger {
		public:
			SeverityLogger( int i_Severity = 0 )
			: m_Severity( i_Severity )
			{}
			
			virtual ~SeverityLogger() {}
			
			void SetSeverity( int i_Severity ) {
				m_Severity = i_Severity;
			}
			
			int Severity() {
				return m_Severity; 
			}
			
			virtual void Log( const std::string& i_Message ) {
				for( std::vector<Sink::BaseSink*>::iterator it = m_Sinks.begin(); it != m_Sinks.end(); ++it ){
					(*it)->Write( i_Message );
				}
				
			}
			
			virtual void Log( int i_Severity, const std::string& i_Message ) {
				if( i_Severity > (m_Severity - 1) ){
					for( std::vector<Sink::BaseSink*>::iterator it = m_Sinks.begin(); it != m_Sinks.end(); ++it ){
						(*it)->Write( i_Message );
					}
					
				}
				
			}
			
		private:
			int m_Severity;
			
		};
		
	} // namespace Log
	
} // namespace NDKit

#endif // _NDKit_Log_Logger_h
